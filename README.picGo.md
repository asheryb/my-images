

# PicGo 配置Gitee 图床

## 1.下载安装picGo

picGo下载地址：[Releases · Molunerfinn/PicGo · GitHub](https://github.com/Molunerfinn/PicGo/releases)

## 2.打开picGo详情页面

+ 打开 PicGo ，选择 插件设置，搜索 gitee，推荐选择第二个。

  ![](https://gitee.com/asheryb/my-images/raw/master/PicGo/21-05-27/1.png)

+ 点击 PicGo 设置，滑到最下方，如果你只使用 Gitee 做图床的话，可以取消其他图床的对勾（可选）。

  ![](https://gitee.com/asheryb/my-images/raw/master/PicGo/21-05-27/2.png)

  

  ![](https://gitee.com/asheryb/my-images/raw/master/PicGo/21-05-27/3.png)

## 3. 配置picGo

![](https://gitee.com/asheryb/my-images/raw/master/PicGo/21-05-27/4.png)

+ repo：用户名/仓库名称，比如我自己的仓库jsnucrh/blogImage，也可以直接复制仓库的url

+ branch：分支，这里写上master

+ token：填入码云的私人令牌

+ path：路径，一般写上img

+ customPath：提交消息，这一项和下一项customURL都不用填。在提交到码云后，会显示提交消息，插件默认提交的是 Upload 图片名 by picGo - 时间

## 4.获取gitee token信息

+ Gitee 页面，点击你的头像，点击设置。

  ![](https://gitee.com/asheryb/my-images/raw/master/PicGo/21-05-27/6.png)

+ 点击私人令牌，选择生成新令牌。

  ![](https://gitee.com/asheryb/my-images/raw/master/PicGo/21-05-27/7.png)

+ 填写令牌描述，选择权限，点击提交。

  ![](https://gitee.com/asheryb/my-images/raw/master/PicGo/21-05-27/8.png)

+ 输入你的码云密码，验证操作。

  ![](https://gitee.com/asheryb/my-images/raw/master/PicGo/21-05-27/9.png)

+ 生成令牌后点击复制，关闭这个窗口之后就再也看不到这个令牌内容了。

  ![](https://gitee.com/asheryb/my-images/raw/master/PicGo/21-05-27/10.png)

+ 回到PicGo，将复制的令牌粘贴到token中，点击确定，点击设置为默认图床。

  ![](https://gitee.com/asheryb/my-images/raw/master/PicGo/21-05-27/11.png)

## 5.与typora搭配使用

![](https://gitee.com/asheryb/my-images/raw/master/PicGo/21-05-27/5.png)









