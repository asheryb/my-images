

![4d4yok](https://gitee.com/asheryb/my-images/raw/master/uPic/4d4yok.png)

# uPic：支持自定义，一款免费而强大的Mac图床客户端

对于markdown重度用户而言，一款好用的图床工具能够有效提升效率。从Windows到Mac，笔者也算是辗转用过多款图床客户端，包括iPic、PicGo等等。而自从更换新的工作环境之后，笔者对于图床客户端又有了新的需求——自定义图床。

> uPic下载链接：https://github.com/gee1k/uPic/releases



uPic是一款开源简洁的图床客户端，方便用户直接上传图片到指定的存储空间，并获取有效的markdown图片地址，其默认图床为SM.MS，当然也支持配置腾讯云、七牛云、又拍云等常见的对象存储。最关键的是支持图床自定义配置，也让我毅然抛弃了一直以来付费使用的iPic。

![5d3b30842ec0351451](https://gitee.com/asheryb/my-images/raw/master/uPic/5d3b30842ec0351451.png)

> 直到最新发布的v0.8.0版本，uPic已经成为一款非常成熟且强大的图床客户端，且安装包大小也不过才10.5M。

uPic目前仅支持Mac端，默认上传到SM.MS匿名图床，除此之外还支持微博、码云、Github以及腾讯云、阿里云、又拍云、七牛云总共八个图床。其中，由于微博近期已经陆续开启防盗链，笔者不建议在个人博客中使用。

![Ku12Lo](https://gitee.com/asheryb/my-images/raw/master/uPic/Ku12Lo.png)

![5d3b1b5854a3588994](https://gitee.com/asheryb/my-images/raw/master/uPic/5d3b1b5854a3588994.gif)

在上传方式上，uPic支持本地选择/复制文件上传、截图上传，甚至可直接拖动文件到菜单栏uPic图标上完成上传动作。 

在最新更新v0.8.0版本之后，uPic新增支持直接从浏览器中复制或者拖动在线图片上传到指定图床，这样就不用下载到本地再进行上传。

![uS3zD2](https://gitee.com/asheryb/my-images/raw/master/uPic/uS3zD2.gif)

不过需要注意的是，GIF动图不大一样，直接从浏览器中复制或者拖动GIF上传至uPic图床，可能最终上传的只是GIF图中一帧的画面，所以对于GIF最好还是先下载到本地，然后使用uPic上传到图床。

在“设置——拓展——访达拓展”中勾选uPic一项，即可开启本地文件右键菜单中“使用uPic上传”的功能，也算是比较方便的一种本地文件上传方式。

![AYC0Pu](https://gitee.com/asheryb/my-images/raw/master/uPic/AYC0Pu.png)

![O0ZYGh](https://gitee.com/asheryb/my-images/raw/master/uPic/O0ZYGh.png)

一旦完成上传动作，所输出的文件链接会自动复制到剪切板，直接在文档中粘贴即可，在输出链接格式上可根据需求选择URL、HTML、Markdown三种。

![l0HrDi](https://gitee.com/asheryb/my-images/raw/master/uPic/l0HrDi.png)

点击菜单栏uPic图标，还可以查看最近十条上传记录，同时可以预览上传内容缩略图，以便选择重新复制。

![XuNZMA](https://gitee.com/asheryb/my-images/raw/master/uPic/XuNZMA.png)

熟练使用快捷键操作，可以进一步提升使用效率。uPic支持三种上传方式的自定义快捷键。基本不用点击菜单了去选择，即可完成上传动作。

![hESXq7](https://gitee.com/asheryb/my-images/raw/master/uPic/hESXq7.png)

例如，通过设定好的快捷键完成截图上传操作，按住鼠标框选所需区域，松开鼠标之后即完成截图并直接上传到图床（注意没有确认截图的过程哦～）。

![qzbLTB](https://gitee.com/asheryb/my-images/raw/master/uPic/qzbLTB.gif)

