# upic+gitee图床，自由书写Markdown

## 使用的软件

Typora：Markdown文档编辑器([www.typora.io/](https://www.typora.io/))

upic：图床工具([github.com/gee1k/uPic](https://github.com/gee1k/uPic))

Gitee/GitHub:网上有很多GitHub了，**Gitee存在容量限制，忘记是多大了，使用是注意一下**

# 创建自己的Gitee图床

## 1 创建账号

> [gitee.com/,自行创建账号就可以了](https://gitee.com/,自行创建账号就可以了)
>
> 和github很相似，但是速度更快

## 2 创建仓库

内容按照自己的习惯

![17181d4ea4e99ec0](https://gitee.com/asheryb/my-images/raw/master/uPic/17181d4ea4e99ec0.jpg)

> 注意，仓库要设置为**私有**

## 3 创建token

点击**设置**，选择私人令牌

![17181d4ea51624b0](https://gitee.com/asheryb/my-images/raw/master/uPic/17181d4ea51624b0.jpg)

此时注意**保存**一下

![17181d4ea6c6cc12](https://gitee.com/asheryb/my-images/raw/master/uPic/17181d4ea6c6cc12.jpg)

![17181d4ea5fdf3de](https://gitee.com/asheryb/my-images/raw/master/uPic/17181d4ea5fdf3de.jpg)

> 此时已经创建完成，可以使用了，创建成功后，会生成一串token，这串token之后不会再显示，所以第一次看到的时候，就要好好保存

# 配置uPic

## 1 下载地址

> uPic下载链接：[github.com/gee1k/uPic/…](https://github.com/gee1k/uPic/releases)

## 2 支持的格式

![17181d4ea6255d6f](https://gitee.com/asheryb/my-images/raw/master/uPic/17181d4ea6255d6f.jpg)

## 3 配置Gitee

![17181d4edf6a7475](https://gitee.com/asheryb/my-images/raw/master/uPic/17181d4edf6a7475.jpg)

> 可以上传一张图片测试一下，直接登录gitee后台，查看是否上传成功**压缩**要开启，因为gitee超过1M图片就要登录才能查看

# 配置Typora

## 1修改下图像配置就可以了

![17181d4ed39db610](https://gitee.com/asheryb/my-images/raw/master/uPic/17181d4ed39db610.jpg)